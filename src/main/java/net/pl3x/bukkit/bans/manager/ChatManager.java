package net.pl3x.bukkit.bans.manager;

import net.pl3x.bukkit.bans.configuration.Config;
import net.pl3x.bukkit.bans.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ChatManager {
    public static void sendMessage(CommandSender sender, Lang message) {
        sendMessage(sender, message.toString());
    }

    public static void sendMessage(CommandSender sender, String message) {
        if (message == null || ChatColor.stripColor(message).equals("")) {
            return;
        }
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
    }

    public static void broadcastAction(String broadcast, CommandSender sender) {
        Set<CommandSender> recipients = new HashSet<>();
        recipients.add(Bukkit.getConsoleSender()); // always include console
        if (sender != null) {
            recipients.add(sender); // always include command sender if provided
        }

        if (Config.BROADCAST_BANS.getBoolean()) {
            // config says broadcast to everyone
            recipients.addAll(Bukkit.getOnlinePlayers());
        } else {
            // config says only broadcast to those with permission
            recipients.addAll(
                    Bukkit.getOnlinePlayers().stream()
                            .filter(player -> player.hasPermission("see.ban.broadcasts"))
                            .collect(Collectors.toList()));
        }

        // broadcast the ban
        for (CommandSender recipient : recipients) {
            ChatManager.sendMessage(recipient, broadcast);
        }
    }
}
