package net.pl3x.bukkit.bans.manager;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.pl3x.bukkit.chatapi.ComponentSender;
import org.bukkit.entity.Player;

public class SpigotChatManager extends ChatManager {
    public static TextComponent compileTooltip(String text, String tooltip) {
        TextComponent component = new TextComponent(colorize("&e" + text));
        component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, colorize(tooltip)));
        return component;
    }

    public static BaseComponent[] colorize(String text) {
        return TextComponent.fromLegacyText(ChatColor.translateAlternateColorCodes('&', text));
    }

    public static void sendMessage(Player player, BaseComponent message) {
        if (message == null || ChatColor.stripColor(TextComponent.toLegacyText(message)).equals("")) {
            return;
        }
        ComponentSender.sendMessage(player, message);
    }
}
