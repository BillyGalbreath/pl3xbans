package net.pl3x.bukkit.bans;

import net.pl3x.bukkit.bans.configuration.Config;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeUtils {
    private static final long YEAR = 60 * 60 * 24 * 365;
    private static final long MONTH = 60 * 60 * 24 * 30;
    private static final long WEEK = 60 * 60 * 24 * 7;
    private static final long DAY = 60 * 60 * 24;
    private static final long HOUR = 60 * 60;
    private static final long MINUTE = 60;

    public static long timeToSeconds(String time) {
        long seconds = 0;
        try {
            seconds = Long.parseLong(time);
        } catch (NumberFormatException e) {
            Matcher matcher = Pattern.compile("\\d+\\D+").matcher(time);
            while (matcher.find()) {
                String[] split = matcher.group().split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
                long num = Long.parseLong(split[0]);
                switch (split[1]) {
                    case "s":
                        seconds += num;
                        break;
                    case "m":
                        seconds += num * MINUTE;
                        break;
                    case "h":
                        seconds += num * HOUR;
                        break;
                    case "d":
                        seconds += num * DAY;
                        break;
                    case "w":
                        seconds += num * WEEK;
                        break;
                    case "M":
                        seconds += num * MONTH;
                        break;
                    case "y":
                        seconds += num * YEAR;
                        break;
                }
            }
        }
        return seconds;
    }

    public static String secondsToTime(long seconds) {
        long years = seconds / YEAR;
        seconds -= years * YEAR;
        long months = seconds / MONTH;
        seconds -= months * MONTH;
        long weeks = seconds / WEEK;
        seconds -= weeks * WEEK;
        long days = seconds / DAY;
        seconds -= days * DAY;
        long hours = seconds / HOUR;
        seconds -= hours * HOUR;
        long minutes = seconds / MINUTE;
        seconds -= minutes * MINUTE;

        StringBuilder sb = new StringBuilder();
        if (years > 0) {
            sb.append(years).append(" year").append(years > 1 ? "s " : " ");
        }
        if (months > 0) {
            sb.append(months).append(" month").append(months > 1 ? "s " : " ");
        }
        if (weeks > 0) {
            sb.append(weeks).append(" week").append(weeks > 1 ? "s " : " ");
        }
        if (days > 0) {
            sb.append(days).append(" day").append(days > 1 ? "s " : " ");
        }
        if (hours > 0) {
            sb.append(hours).append(" hour").append(hours > 1 ? "s " : " ");
        }
        if (minutes > 0) {
            sb.append(minutes).append(" minute").append(minutes > 1 ? "s " : " ");
        }
        sb.append(seconds).append(" second").append(seconds > 1 ? "s " : " ");

        return sb.toString().trim();
    }

    public static String epochToDate(String epoch) {
        SimpleDateFormat sdf = new SimpleDateFormat(Config.DATE_FORMAT.getString());
        return sdf.format(new Date(Long.parseLong(epoch) * 1000));
    }
}
