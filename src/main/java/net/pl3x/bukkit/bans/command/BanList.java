package net.pl3x.bukkit.bans.command;

import net.pl3x.bukkit.bans.Pl3xBans;
import net.pl3x.bukkit.bans.configuration.Config;
import net.pl3x.bukkit.bans.configuration.Lang;
import net.pl3x.bukkit.bans.manager.ChatManager;
import net.pl3x.bukkit.bans.task.CompileBanList;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;

public class BanList implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("command.banlist")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        int page = 0;
        if (args.length > 0) {
            try {
                page = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                ChatManager.sendMessage(sender, Lang.INVALID_PAGE_NUMBER);
                return true;
            }
        }

        // use tooltips?
        boolean useTooltips = false;
        try {
            Class.forName("net.md_5.bungee.api.chat.HoverEvent");
            if (Config.USE_CHAT_TOOLTIPS.getBoolean() && sender instanceof Player) {
                useTooltips = true;
            }
        } catch (ClassNotFoundException ignore) {
        }

        ChatManager.sendMessage(sender, Lang.PLEASE_WAIT);

        new CompileBanList(sender, useTooltips, page).runTaskAsynchronously(Pl3xBans.getPlugin(Pl3xBans.class));
        return true;
    }
}
