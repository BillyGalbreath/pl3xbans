package net.pl3x.bukkit.bans.command;

import net.pl3x.bukkit.bans.Logger;
import net.pl3x.bukkit.bans.configuration.BanConfig;
import net.pl3x.bukkit.bans.configuration.Lang;
import net.pl3x.bukkit.bans.manager.ChatManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.List;
import java.util.UUID;

public class Pardon implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("command.pardon")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        // check arguments
        if (args.length == 0) {
            ChatManager.sendMessage(sender, Lang.INVALID_COMMAND);
            return false;
        }

        // get target (player to unban)
        OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
        if (target == null) {
            ChatManager.sendMessage(sender, Lang.PLAYER_NOT_FOUND);
            return true;
        }

        // setup some variables
        UUID uuid = target.getUniqueId();
        String name = target.getName();
        String key = uuid.toString();

        // check if target is currently banned
        BanConfig banConfig = BanConfig.getConfig();
        if (!banConfig.safeIsSet(key)) {
            ChatManager.sendMessage(sender, Lang.IS_NOT_BANNED.replace("{target}", name));
            return true;
        }

        // remove from bans.yml
        banConfig.safeSet(key, null);
        banConfig.save();

        // debug to console
        Logger.debug("&3Pardoned Player!");
        Logger.debug(" &e- &3Name: &e" + name);
        Logger.debug(" &e- &3UUID: &e" + uuid);

        // broadcast
        // broadcast ban message
        ChatManager.broadcastAction(
                Lang.BROADCAST_PARDON
                        .replace("{target}", name)
                        .replace("{name}", name)
                        .replace("{uuid}", uuid.toString()),
                sender);
        return true;
    }
}
