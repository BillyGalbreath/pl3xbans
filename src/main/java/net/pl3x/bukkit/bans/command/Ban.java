package net.pl3x.bukkit.bans.command;

import net.pl3x.bukkit.bans.Logger;
import net.pl3x.bukkit.bans.TimeUtils;
import net.pl3x.bukkit.bans.configuration.BanConfig;
import net.pl3x.bukkit.bans.configuration.Config;
import net.pl3x.bukkit.bans.configuration.HistoryConfig;
import net.pl3x.bukkit.bans.configuration.Lang;
import net.pl3x.bukkit.bans.manager.ChatManager;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class Ban implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("command.ban")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        // check arguments
        if (args.length == 0) {
            ChatManager.sendMessage(sender, Lang.INVALID_COMMAND);
            return false;
        }

        // check if reload
        if (args[0].equalsIgnoreCase("reload")) {
            Config.reload();
            Lang.reload(true);
            BanConfig.reload();
            HistoryConfig.removeConfigs();
            ChatManager.sendMessage(sender, Lang.CONFIGS_RELOADED);
            return true;
        }

        // get target (player to ban)
        OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
        if (target == null) {
            ChatManager.sendMessage(sender, Lang.PLAYER_NOT_FOUND);
            return true;
        }

        // setup some variables
        String name = target.getName();
        UUID uuid = target.getUniqueId();
        String key = uuid.toString();

        // check if target is already banned
        BanConfig banConfig = BanConfig.getConfig();
        if (banConfig.safeIsSet(key)) {
            ChatManager.sendMessage(sender, Lang.ALREADY_BANNED.replace("{target}", name));
            return true;
        }

        // setup more variables
        long seconds = 0;
        long time = System.currentTimeMillis() / 1000;
        String reason = null;

        // check for duration and reason
        if (args.length > 1) {
            seconds = TimeUtils.timeToSeconds(args[1]);
            if (args.length == 2 && seconds == 0) {
                reason = StringUtils.join(args, " ", 1, args.length);
            } else if (args.length > 2) {
                reason = StringUtils.join(args, " ", seconds == 0 ? 1 : 2, args.length);
            }
        }

        if ((reason == null || reason.trim().equals("")) && Config.REQUIRE_REASON.getBoolean()) {
            ChatManager.sendMessage(sender, Lang.REASON_REQUIRED);
            return true;
        } else {
            reason = Lang.DEFAULT_REASON.toString();
        }

        // add to bans.yml
        banConfig.safeSet(key + ".name", name);
        banConfig.safeSet(key + ".uuid", uuid.toString());
        banConfig.safeSet(key + ".at", time);
        banConfig.safeSet(key + ".until", seconds == 0 ? 0 : time + seconds); // set 0 for perm ban
        banConfig.save();

        // add to history
        HistoryConfig historyConfig = HistoryConfig.getConfig(uuid);
        historyConfig.set(time + ".name", name);
        historyConfig.set(time + ".uuid", uuid.toString());
        historyConfig.set(time + ".duration", seconds);
        historyConfig.set(time + ".reason", reason);
        historyConfig.set(time + ".bywho", sender.getName());
        historyConfig.set(time + ".type", "account ban");
        historyConfig.save();

        // debug to console
        Logger.debug("&3Banned Player!");
        Logger.debug(" &e- &3Name: &e" + name);
        Logger.debug(" &e- &3UUID: &e" + uuid);
        Logger.debug(" &e- &3Duration: &e" + (seconds == 0 ? "permanent" : seconds + " seconds"));
        Logger.debug(" &e- &3Reason: &e" + reason);
        Logger.debug(" &e- &3By Who: &e" + sender.getName());
        Logger.debug(" &e- &3At: &e" + time);

        // kick player if online
        Player onlinePlayer = Bukkit.getPlayer(uuid);
        if (onlinePlayer != null) {
            onlinePlayer.kickPlayer(ChatColor.translateAlternateColorCodes('&', Lang.KICK_MESSAGE.replace("{reason}", reason)));
        }

        // broadcast ban message
        ChatManager.broadcastAction(
                (seconds > 0 ? Lang.BROADCAST_TIMED_BAN : Lang.BROADCAST_BAN)
                        .replace("{target}", name)
                        .replace("{name}", name)
                        .replace("{uuid}", uuid.toString())
                        .replace("{duration}", TimeUtils.secondsToTime(seconds))
                        .replace("{reason}", reason)
                        .replace("{by-who}", sender.getName())
                        .replace("{at}", Long.toString(time)),
                sender);
        return true;
    }
}
