package net.pl3x.bukkit.bans.command;

import net.pl3x.bukkit.bans.configuration.Lang;
import net.pl3x.bukkit.bans.manager.ChatManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.List;

public class BanHistory implements TabExecutor {
    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!sender.hasPermission("command.banhistory")) {
            ChatManager.sendMessage(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        // usage: /banhistory player

        return false;
    }
}
