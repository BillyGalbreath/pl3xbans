package net.pl3x.bukkit.bans.configuration;

import net.pl3x.bukkit.bans.Pl3xBans;

public enum Config {
    COLOR_LOGS(true),
    DEBUG_MODE(false),
    LANGUAGE_FILE("lang-en.yml"),
    REQUIRE_REASON(false),
    BROADCAST_BANS(true),
    AUTO_PARDON_TIMER(60),
    USE_CHAT_TOOLTIPS(true),
    DATE_FORMAT("MM/dd/yyyy HH:mm:ss");

    private final Pl3xBans plugin;
    private final Object def;

    Config(Object def) {
        this.plugin = Pl3xBans.getPlugin(Pl3xBans.class);
        this.def = def;
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    public String getString() {
        return plugin.getConfig().getString(getKey(), (String) def);
    }

    public boolean getBoolean() {
        return plugin.getConfig().getBoolean(getKey(), (Boolean) def);
    }

    public int getInt() {
        return plugin.getConfig().getInt(getKey(), (int) def);
    }

    public static void reload() {
        Pl3xBans.getPlugin(Pl3xBans.class).reloadConfig();
    }
}
