package net.pl3x.bukkit.bans.configuration;

import net.pl3x.bukkit.bans.Pl3xBans;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.Set;

public class BanConfig extends YamlConfiguration {
    private static BanConfig config;

    public static BanConfig getConfig() {
        if (config == null) {
            config = new BanConfig();
        }
        return config;
    }

    public static void reload() {
        config = null;
        getConfig();
    }

    private final File file;
    private final Object saveLock = new Object();

    private BanConfig() {
        super();
        file = new File(Pl3xBans.getPlugin(Pl3xBans.class).getDataFolder(), "bans.yml");
        load();
    }

    private void load() {
        synchronized (saveLock) {
            try {
                this.load(file);
            } catch (Exception ignore) {
            }
        }
    }

    public void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    public Set<String> safeGetKeys() {
        synchronized (saveLock) {
            return getKeys(false);
        }
    }

    public boolean safeIsSet(String key) {
        synchronized (saveLock) {
            return isSet(key);
        }
    }

    public String safeGetString(String key) {
        synchronized (saveLock) {
            return getString(key);
        }
    }

    public long safeGetLong(String key) {
        synchronized (saveLock) {
            return getLong(key, 0);
        }
    }

    public int safeGetInt(String key) {
        synchronized (saveLock) {
            return getInt(key, 0);
        }
    }

    public void safeSet(String key, Object value) {
        synchronized (saveLock) {
            set(key, value);
        }
    }
}
