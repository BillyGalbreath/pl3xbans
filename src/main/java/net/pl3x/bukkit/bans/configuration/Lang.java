package net.pl3x.bukkit.bans.configuration;

import net.pl3x.bukkit.bans.Logger;
import net.pl3x.bukkit.bans.Pl3xBans;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Lang {
    COMMAND_NO_PERMISSION("&4You do not have permission for this command!"),
    PLAYER_NOT_FOUND("&4Cannot find that player!"),
    INVALID_COMMAND("&4Invalid command structure!"),
    INVALID_PAGE_NUMBER("&4Page number invalid!"),
    PLEASE_WAIT("&dPlease wait..."),
    ALREADY_BANNED("&4{target} is already banned!"),
    IS_NOT_BANNED("&4{target} is not banned!"),
    REASON_REQUIRED("&4Reason is required!"),
    CONFIGS_RELOADED("&dReloaded Pl3xBans configs."),
    BROADCAST_BAN("&e{target} has been banned."),
    BROADCAST_TIMED_BAN("&e{target} has been banned for {duration}."),
    BROADCAST_PARDON("&e{target} has been pardoned."),
    BROADCAST_AUTO_PARDON("&eTimed ban has expired for {target}."),
    DEFAULT_REASON("no reason specified"),
    KICK_MESSAGE("&4&lYou Are Banned\n&r&7{reason}"),
    BANNED_LIST("&dBanned List (page {page})\n{list}"),
    BANNED_LIST_ENTRY("&e{key} &don &e{at} &dexpires &e{until}"),
    CHAT_TOOLTIP_FORMAT("&eName: &7{name}\n&eUUID: &7{uuid}\n&eIP: &7{ip}\n&eAt: &7{at}\n&eUntil: &7{until}");

    private final String def;

    private static File configFile;
    private static FileConfiguration config;

    Lang(String def) {
        this.def = def;
        reload();
    }

    public static void reload() {
        reload(false);
    }

    public static void reload(boolean force) {
        if (configFile == null || force) {
            String lang = Config.LANGUAGE_FILE.getString();
            Logger.debug("Loading language file: " + lang);
            configFile = new File(Pl3xBans.getPlugin(Pl3xBans.class).getDataFolder(), lang);
            if (!configFile.exists()) {
                Pl3xBans.getPlugin(Pl3xBans.class).saveResource(Config.LANGUAGE_FILE.getString(), false);
            }
        }
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    private String getKey() {
        return name().toLowerCase().replace("_", "-");
    }

    @Override
    public String toString() {
        String value = config.getString(name());
        if (value == null) {
            value = config.getString(getKey());
        }
        if (value == null) {
            Logger.warn("Missing lang data in file: " + getKey());
            value = def;
        }
        if (value == null) {
            Logger.error("Missing default lang data: " + getKey());
            value = "&c[missing lang data]";
        }
        return ChatColor.translateAlternateColorCodes('&', value);
    }

    public String replace(String find, String replace) {
        return toString().replace(find, replace);
    }
}
