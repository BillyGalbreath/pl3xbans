package net.pl3x.bukkit.bans.configuration;

import net.pl3x.bukkit.bans.Pl3xBans;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class HistoryConfig extends YamlConfiguration {
    private static final Map<UUID, HistoryConfig> configs = new HashMap<>();

    public static HistoryConfig getConfig(UUID uuid) {
        synchronized (configs) {
            if (configs.containsKey(uuid)) {
                return configs.get(uuid);
            }
            HistoryConfig config = new HistoryConfig(uuid);
            configs.put(uuid, config);
            return config;
        }
    }

    public static void removeConfigs() {
        Collection<HistoryConfig> oldConfs = new ArrayList<>(configs.values());
        synchronized (configs) {
            oldConfs.forEach(HistoryConfig::discard);
        }
    }

    private File file = null;
    private final Object saveLock = new Object();
    private final UUID uuid;

    private HistoryConfig(UUID uuid) {
        super();
        file = new File(Pl3xBans.getPlugin(Pl3xBans.class).getDataFolder(), "history" + File.separator + uuid.toString() + ".yml");
        this.uuid = uuid;
        reload();
    }

    private void reload() {
        synchronized (saveLock) {
            try {
                load(file);
            } catch (Exception ignore) {
            }
        }
    }

    public void save() {
        synchronized (saveLock) {
            try {
                save(file);
            } catch (Exception ignore) {
            }
        }
    }

    private void discard() {
        synchronized (configs) {
            configs.remove(uuid);
        }
    }
}