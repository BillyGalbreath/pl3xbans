package net.pl3x.bukkit.bans;

import net.pl3x.bukkit.bans.command.Ban;
import net.pl3x.bukkit.bans.command.BanHistory;
import net.pl3x.bukkit.bans.command.BanList;
import net.pl3x.bukkit.bans.command.Pardon;
import net.pl3x.bukkit.bans.configuration.Config;
import net.pl3x.bukkit.bans.configuration.Lang;
import net.pl3x.bukkit.bans.listener.PlayerListener;
import net.pl3x.bukkit.bans.task.AutoPardon;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xBans extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        Lang.reload();

        PluginManager pm = Bukkit.getPluginManager();

        getCommand("ban").setExecutor(new Ban());
        getCommand("banhistory").setExecutor(new BanHistory());
        getCommand("ban-ip").setExecutor(this);
        getCommand("banlist").setExecutor(new BanList());
        getCommand("pardon").setExecutor(new Pardon());
        getCommand("pardon-ip").setExecutor(this);

        pm.registerEvents(new PlayerListener(), this);

        new AutoPardon().runTaskTimer(this, 100L, Config.AUTO_PARDON_TIMER.getInt() * 20L);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4IP bans are unavailable in this version."));
        return true;
    }
}
