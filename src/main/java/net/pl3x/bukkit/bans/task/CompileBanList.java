package net.pl3x.bukkit.bans.task;

import net.pl3x.bukkit.bans.Pl3xBans;
import net.pl3x.bukkit.bans.configuration.BanConfig;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompileBanList extends BukkitRunnable {
    private final CommandSender sender;
    private final boolean useTooltips;
    private final int page;

    public CompileBanList(CommandSender sender, boolean useTooltips, int page) {
        this.sender = sender;
        this.useTooltips = useTooltips;
        this.page = page;
    }

    @Override
    public void run() {
        BanConfig banConfig = BanConfig.getConfig();

        List<String> keys = new ArrayList<>(banConfig.safeGetKeys());
        Collections.sort(keys);

        int count = 0;
        int perPage = 5; // TODO better pagination
        int perLine = 256; // TODO for tooltip bans (max chat length per message excluding json/hover)

        List<Map<String, String>> entries = new ArrayList<>();
        for (int i = page * perPage; count < perPage && i < keys.size(); i++) {
            Map<String, String> entry = new HashMap<>();

            entry.put("key", keys.get(i));
            entry.put("name", banConfig.safeGetString(keys.get(i) + ".name"));
            entry.put("uuid", banConfig.safeGetString(keys.get(i) + ".uuid"));
            entry.put("ip", banConfig.safeGetString(keys.get(i) + ".ip"));
            entry.put("at", banConfig.safeGetString(keys.get(i) + ".at"));
            entry.put("until", banConfig.safeGetString(keys.get(i) + ".until"));

            entries.add(entry);
        }

        if (useTooltips) {
            new BanListTooltipResponse((Player) sender, page, entries).runTask(Pl3xBans.getPlugin(Pl3xBans.class));
        } else {
            new BanListResponse(sender, page, entries).runTask(Pl3xBans.getPlugin(Pl3xBans.class));
        }
    }
}
