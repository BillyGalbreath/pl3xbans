package net.pl3x.bukkit.bans.task;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.pl3x.bukkit.bans.TimeUtils;
import net.pl3x.bukkit.bans.configuration.Lang;
import net.pl3x.bukkit.bans.manager.SpigotChatManager;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class BanListTooltipResponse extends BukkitRunnable {
    private final Player player;
    private final int page;
    private final List<Map<String, String>> entries;

    public BanListTooltipResponse(Player player, int page, List<Map<String, String>> entries) {
        this.player = player;
        this.page = page;
        this.entries = entries;
    }

    @Override
    public void run() {
        // compile response
        List<BaseComponent> names = new ArrayList<>();
        for (Map<String, String> entry : entries) {
            String ipOrName = entry.get("key").contains("-") ? entry.get("name") : entry.get("key");
            String tooltip = Lang.CHAT_TOOLTIP_FORMAT
                    .replace("{key}", ipOrName)
                    .replace("{name}", entry.get("name"))
                    .replace("{uuid}", entry.get("uuid"))
                    .replace("{ip}", entry.get("ip"))
                    .replace("{at}", TimeUtils.epochToDate(entry.get("at")))
                    .replace("{until}", entry.get("until").equals("0") ? "never" : TimeUtils.epochToDate(entry.get("until")));
            names.add(SpigotChatManager.compileTooltip(ipOrName, tooltip));
        }

        // send response
        SpigotChatManager.sendMessage(player, Lang.BANNED_LIST.replace("{page}", Integer.toString(page)).replace("{list}", ""));

        int counter = 0;
        BaseComponent response = new TextComponent("");
        for (BaseComponent name : names) {
            if (counter > 0) {
                response.addExtra(new TextComponent(SpigotChatManager.colorize("&7, ")));
            }
            response.addExtra(name);
            counter++;
            if (counter >= 10) {
                SpigotChatManager.sendMessage(player, response);
                response = new TextComponent("");
                counter = 0;
            }
        }
        SpigotChatManager.sendMessage(player, response);
    }
}
