package net.pl3x.bukkit.bans.task;

import net.pl3x.bukkit.bans.TimeUtils;
import net.pl3x.bukkit.bans.configuration.Lang;
import net.pl3x.bukkit.bans.manager.ChatManager;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.Map;

class BanListResponse extends BukkitRunnable {
    private final CommandSender sender;
    private final int page;
    private final List<Map<String, String>> entries;

    public BanListResponse(CommandSender sender, int page, List<Map<String, String>> entries) {
        this.sender = sender;
        this.page = page;
        this.entries = entries;
    }

    @Override
    public void run() {
        // compile response
        String entriesList = "";
        for (Map<String, String> entry : entries) {
            entriesList += Lang.BANNED_LIST_ENTRY
                    .replace("{key}", entry.get("key").contains("-") ? entry.get("name") : entry.get("key"))
                    .replace("{name}", entry.get("name"))
                    .replace("{uuid}", entry.get("uuid"))
                    .replace("{ip}", entry.get("ip"))
                    .replace("{at}", TimeUtils.epochToDate(entry.get("at")))
                    .replace("{until}", entry.get("until").equals("0") ? "never" : TimeUtils.epochToDate(entry.get("until")))
            + "\n"; // newline between entries
        }

        String[] response = Lang.BANNED_LIST
                .replace("{page}", Integer.toString(page))
                .replace("{list}", entriesList)
                .split("\n");

        // send full response
        for (String message : response) {
            ChatManager.sendMessage(sender, message);
        }
    }
}
