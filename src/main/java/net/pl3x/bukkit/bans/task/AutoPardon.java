package net.pl3x.bukkit.bans.task;

import net.pl3x.bukkit.bans.configuration.BanConfig;
import net.pl3x.bukkit.bans.configuration.Lang;
import net.pl3x.bukkit.bans.manager.ChatManager;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Set;

public class AutoPardon extends BukkitRunnable {
    @Override
    public void run() {
        BanConfig banConfig = BanConfig.getConfig();
        long now = System.currentTimeMillis() / 1000;
        Set<String> keys = banConfig.safeGetKeys();
        for (String key : keys) {
            // check time
            long until = banConfig.safeGetLong(key + ".until");
            if (until < 1 || until > now) {
                continue; // perm banned or ban not expired. ignore.
            }

            // unban
            String name = banConfig.safeGetString(key + ".name");
            banConfig.safeSet(key, null);
            banConfig.save();

            // broadcast
            ChatManager.broadcastAction(Lang.BROADCAST_AUTO_PARDON.replace("{target}", key.contains("-") ? name : key), null);
        }
    }
}
