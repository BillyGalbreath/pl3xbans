package net.pl3x.bukkit.bans.listener;

import net.pl3x.bukkit.bans.Logger;
import net.pl3x.bukkit.bans.configuration.BanConfig;
import net.pl3x.bukkit.bans.configuration.HistoryConfig;
import net.pl3x.bukkit.bans.configuration.Lang;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.UUID;

public class PlayerListener implements Listener {
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();
        String ip = event.getAddress().getHostAddress();

        // check if banned
        BanConfig banConfig = BanConfig.getConfig();
        String key;
        if (banConfig.safeIsSet(uuid.toString())) {
            key = uuid.toString();
        } else if (banConfig.safeIsSet(ip)) {
            key = ip;
        } else {
            return; // not currently banned
        }

        // check time
        long now = System.currentTimeMillis() / 1000;
        long until = banConfig.safeGetInt(key + ".until");
        if (until > 0 && until < now) {
            banConfig.safeSet(key, null);
            banConfig.save();
            Logger.debug("Forced pardon (ban time expired)");
            return; // unbanned
        }

        // set event result to kick banned with reason
        HistoryConfig historyConfig = HistoryConfig.getConfig(uuid);
        String reason = historyConfig.getString(banConfig.safeGetLong(key + ".at") + ".reason", Lang.DEFAULT_REASON.toString());
        event.setResult(PlayerLoginEvent.Result.KICK_BANNED);
        event.setKickMessage(ChatColor.translateAlternateColorCodes('&', Lang.KICK_MESSAGE.replace("{reason}", reason)));
    }
}
